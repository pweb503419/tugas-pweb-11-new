<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple PHP Form with Input Results</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            font-family: Arial, sans-serif;
        }
        .container {
            width: 400px;
            padding: 20px;
            border: 1px solid #ccc;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        form {
            margin-bottom: 20px;
        }
        input[type="text"], input[type="email"], select, textarea, input[type="submit"] {
            width: 100%;
            padding: 10px;
            margin: 5px 0;
            box-sizing: border-box;
        }
        textarea {
            height: 100px;
        }
        .error {
            color: red;
        }
        h2 {
            text-align: center;
        }
        .submitted-info {
            background-color: #f9f9f9;
            padding: 10px;
            border: 1px solid #ccc;
        }
    </style>
</head>
<body>
    <div class="container">
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <label for="name">Name:</label>
            <input type="text" id="name" name="name" value="<?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>" required>
            <span class="error"><?php echo isset($errors['name']) ? $errors['name'] : ''; ?></span>

            <label for="email">Email:</label>
            <input type="email" id="email" name="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>" required>
            <span class="error"><?php echo isset($errors['email']) ? $errors['email'] : ''; ?></span>

            <label for="address">Address:</label>
            <textarea id="address" name="address"><?php echo isset($_POST['address']) ? $_POST['address'] : ''; ?></textarea>

            <label for="gender">Gender:</label>
            <select id="gender" name="gender" required>
                <option value="">Select Gender</option>
                <option value="male" <?php echo (isset($_POST['gender']) && $_POST['gender'] == 'male') ? 'selected' : ''; ?>>Male</option>
                <option value="female" <?php echo (isset($_POST['gender']) && $_POST['gender'] == 'female') ? 'selected' : ''; ?>>Female</option>
                <option value="other" <?php echo (isset($_POST['gender']) && $_POST['gender'] == 'other') ? 'selected' : ''; ?>>Other</option>
            </select>
            <span class="error"><?php echo isset($errors['gender']) ? $errors['gender'] : ''; ?></span>

            <label for="age">Age:</label>
            <input type="text" id="age" name="age" value="<?php echo isset($_POST['age']) ? $_POST['age'] : ''; ?>" required>
            <span class="error"><?php echo isset($errors['age']) ? $errors['age'] : ''; ?></span>

            <label for="comment">Comment:</label>
            <textarea id="comment" name="comment"><?php echo isset($_POST['comment']) ? $_POST['comment'] : ''; ?></textarea>

            <input type="submit" value="Submit">
        </form>

        <?php
        // Check if form is submitted
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Initialize an array to store errors
            $errors = array();

            // Validate inputs
            if (empty($_POST['name'])) {
                $errors['name'] = 'Name is required';
            }

            if (empty($_POST['email'])) {
                $errors['email'] = 'Email is required';
            } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = 'Invalid email format';
            }

            if (empty($_POST['gender'])) {
                $errors['gender'] = 'Gender is required';
            }

            if (empty($_POST['age'])) {
                $errors['age'] = 'Age is required';
            } elseif (!is_numeric($_POST['age'])) {
                $errors['age'] = 'Age must be a number';
            }

            // If no errors, display submitted data
            if (empty($errors)) {
                $name = htmlspecialchars($_POST['name']);
                $email = htmlspecialchars($_POST['email']);
                $address = htmlspecialchars($_POST['address']);
                $gender = htmlspecialchars($_POST['gender']);
                $age = htmlspecialchars($_POST['age']);
                $comment = htmlspecialchars($_POST['comment']);

                echo "<div class='submitted-info'>";
                echo "<h2>Submitted Information:</h2>";
                echo "<p><strong>Name:</strong> $name</p>";
                echo "<p><strong>Email:</strong> $email</p>";
                echo "<p><strong>Address:</strong> $address</p>";
                echo "<p><strong>Gender:</strong> $gender</p>";
                echo "<p><strong>Age:</strong> $age</p>";
                echo "<p><strong>Comment:</strong> $comment</p>";
                echo "</div>";
            }
        }
        ?>
    </div>
</body>
</html>
